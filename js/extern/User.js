/**
 * Created by woud on 2014/12/27.
 */

var gUserInfo = {
    textures : {
        mainPhotoTextureUrl : 'textures/image/sunzhongshan.jpg',
        frameTextureUrl : 'textures/image/xiangkuang_bw.png',
        wallTextureUrl : 'textures/wall/qz115.jpg',
        floorTextureUrl : 'textures/ground/wood4.jpg'
    },
    assets : [{
        type : 'mtlobj',
        objPath : 'obj/vases/vase.obj',
        mtlPath : 'obj/vases/vase.mtl'
    }, {
        type : 'mtlobj',
        objPath : 'obj/desk/desk.obj',
        mtlPath : 'obj/desk/desk.mtl'
    }, {
        type : 'texture',
        path : 'textures/image/xiangkuang_bw.png'
    }, {
        type : 'texture',
        path : 'textures/wall/qz115.jpg'
    }, {
        type : 'texture',
        path : 'textures/ground/wood4.jpg'
    }],
    models : {

        //  only for debug
        decorationSideObj : 'obj/vases/vase.obj',
        decorationSideMtl : 'obj/vases/vase.mtl',
        decorationFrontObj : 'obj/desk/desk.obj',
        decorationFrontMtl : 'obj/desk/desk.mtl',

        //  new data structure
        place_1 : {
            position : { x : 1100, y : -250, z : -600},
            object : null
        },
        place_2 : {
            position : { x : 1300, y : -100, z : -535},
            object : null
        },
        place_3 : {
            position : { x : 1100, y : -250, z : 600},
            object : null
        },
        place_4 : {
            position : { x : 1200, y : -60, z : -150},
            object : null
        },
        place_5 : {
            position : { x : 1200, y : -60, z : 150},
            object : null
        }
    }
}