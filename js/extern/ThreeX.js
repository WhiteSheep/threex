/**
 * Created by woud on 2014/12/27.
 */

var THREEX = { };

/////////////////////////////////////////////////////////////////////////////////////////////////////////
/// THREEX is a ex-lib for three.js
///
/// It contains many modules to help user make 3D scene more easily
///
/// 1. Data Structure
///     1.1 Map
/// 2. Global Area
/// 3. Memory Cache
/// 4. Model
/// ------------------------------------------------------------------------------------
/// Coded by Woud, copy right reserved @2014-2015
/////////////////////////////////////////////////////////////////////////////////////////////////////////

/************************************************************
 *  Data Structure
 *************************************************************/

/**
 * Map is the one of the basic data structure
 * @constructor
 */
THREEX.Map = function () {
    this.map = new Object();
    this.length = 0;

    this.size = function() {
        return this.length;
    }

    this.insert = function(key, value) {
        if (typeof(this.map[key]) == 'undefined')
            ++this.length;
        this.map[key] = value;
    }

    this.get = function(key) {
        return this.map[key];
    }

    this.remove = function(key) {
        try {
            delete this.map[key];
            --this.length;
        } catch(e) {
            return e;
        }
    }

    this.contain = function(key) {
        return typeof(this.map[key]) != 'undefined';
    }

    this.each = function (fn) {
        if (typeof(fn) != 'function')
        return;

        for (var key in this.map) {
            fn(key, this.get(key));
        }
    }
}

/**
 * ModelEntity contains necessary information about model, such as Bounding Box, etc.
 * -    obj : model obj info, default 0
 * -    bBox : bounding box info, default 0
 * @constructor
 */
THREEX.ModelEntity = function () {
    this.obj = null;
    this.bBoxModel = null;
    /**
     * clone method
     * @returns {THREEX.ModelEntity}
     */
    this.clone = function() {
        var anoEnt = new THREEX.ModelEntity();
        anoEnt.obj = this.obj.clone();

        return anoEnt;
    }
}


/************************************************************
 *  Global Data and root method
 *************************************************************/


THREEX.safeEquals = function(item, value) {
    var ret = typeof (item) != "undefined";

    if (typeof(value) == "undefined")
        ret = ret && (item == value);

    return ret;
}

THREEX.ifDefined = function(item) {
    return typeof(item) != "undefined";
}

THREEX.initVar = function(item, value) {
    if (!THREEX.ifDefined(item))
        return value;
    else
        return item;
}

/**
 * forceGlobalUpdate means when something in scene changed, all the things in
 * scene will be set to be update
 *
 * close this switch will make the system run faster
 */
THREEX.forceMaterialUpdate = false;
THREEX.materialNeedsUpdate = false;

THREEX.Global = {
    //  It is used to store all key-val in scene
    sceneList : new THREEX.Map(),
    add : function(key, val) {
        THREEX.Global.sceneList.insert(key, val);
    },
    get : function(key) {
        return THREEX.Global.sceneList.get(key);
    },
    remove : function(key) {
        THREEX.Global.sceneList.remove(key);
    },
    contain : function(key) {
        return THREEX.Global.sceneList.contain(key);
    }
};

/************************************************************
 *  Utils
 ************************************************************/


/**
 * AssetsManager - usually only create one Entity
 * @param array
 * @constructor
 */


THREEX.AssetsManager = function(array) {
    this.assets = array;
    this.loadedCount = 0;
    this.totalCount = 0;
    this.onFinishCallback = function() { };
    this.onCallbackPerFrame = function() { };

    this.load = function() {
        THREEX.AssetsManager.finished = false;
        THREEX.AssetsManager.loadedCount = 0;
        THREEX.AssetsManager.totalCount = array.length;
        THREEX.AssetsManager.preload(array, this.onFinishCallback);
    }
}

/**
 * Preload assets and push them into THREEX.MemCache, all assets will be indexed by url
 * @param array, for each item :
 *      type : insist OBJMTL, Texture
 *      parameters : to be ....
 * @onFinishCallback
 * @idx [optical] don't do anything except you know it's meaning
 */
THREEX.AssetsManager.loadedCount = 0;
THREEX.AssetsManager.totalCount = 0;
THREEX.AssetsManager.finished = true;
THREEX.AssetsManager.preload = function(array, onFinishCallback, idx) {
    idx = THREEX.initVar(idx, 0);

    //  when not loading end
    if (idx < array.length) {
        if (array[idx].type == 'mtlobj') {
            THREEX.Model.load(array[idx].objPath, array[idx].mtlPath,function(object) {
                THREEX.MemCache.saveRes(array[idx].objPath, object);
                console.log(THREEX.Debug.getNowTime() + ': ' + array[idx].objPath + 'loading completed ' + (idx + 1) + ',' + array.length);
                console.log(THREEX.Debug.getNowTime() + ': ' + array[idx].mtlPath + 'loading completed ' + (idx + 1) + ',' + array.length);
                ++THREEX.AssetsManager.loadedCount;
                THREEX.AssetsManager.preload(array, onFinishCallback, idx + 1);
            })
        } else if (array[idx].type == 'texture') {
            var texture = THREEX.Texture.load(array[idx].path, function() {
                THREEX.MemCache.saveRes(array[idx].path, texture);
                console.log(THREEX.Debug.getNowTime() + ': ' + array[idx].path + 'loading completed ' + (idx + 1) + ',' + array.length);
                ++THREEX.AssetsManager.loadedCount;
                THREEX.AssetsManager.preload(array, onFinishCallback, idx + 1);
            })
        }
    } else {
        THREEX.AssetsManager.finished = true;
        onFinishCallback();
    }
}

/************************************************************
 *  Debug
 ************************************************************/

THREEX.Debug = {
    enabled : true,
    runtime : function(fn) {
        var startTime = new Date().getTime();
        var ret = fn();
        if (THREEX.Debug.enabled)
            console.log('[' + THREEX.Debug.getNowTime() + '] : function complete in '
                + (new Date().getDate() - startTime) + ' ms');

        return ret;
    },
    getNowTime : function() {
        return new Date().toLocaleTimeString();
    }
}

/************************************************************
 *  Memory Cache
 *  -   Memory Cache is very important, it can save all the resource
 *  you have read, and you don't need to read from file again.
 *
 *  -   In order to distinguish all resources, we use file path as
 *  the key
 *************************************************************/

THREEX.MemCache = {
    cacheMap : new THREEX.Map(),
    saveRes : function(path, obj) {
        if (obj instanceof THREE.Texture)
            THREEX.MemCache.cacheMap.insert(path, obj);
        else
            THREEX.MemCache.cacheMap.insert(path, obj.clone());
    },
    loadRes : function(path) {
        var ret = THREEX.MemCache.cacheMap.get(path);
        if (ret instanceof THREE.Texture)
            return ret;
        else
            return ret.clone();
    },
    checkRes : function(path) {
        return THREEX.MemCache.cacheMap.contain(path);
    }
};

/************************************************************
 *  Texture
 *  -   This part provide the common methods to deal with the texture
 *************************************************************/

THREEX.Texture = { };
THREEX.Texture.load = function(url, onload) {
    var texture;
    var onloadCallback = THREEX.initVar(onload, function() {});

    if (THREEX.MemCache.checkRes(url)) {
        texture = THREEX.MemCache.loadRes(url);
        onloadCallback(texture);
    } else {
        texture = THREE.ImageUtils.loadTexture(url, THREE.UVMapping, onloadCallback);
        THREEX.MemCache.saveRes(url, texture);
    }


    return texture;
}


/************************************************************
 *  Model
 *  -   This part provide the common methods to deal with the model
 *      including import, replace models
 *************************************************************/

THREEX.Model = { };
THREEX.Model.onProgress = function (xhr) {
    if (xhr.lengthComputable) {
        var percentComplete = xhr.loaded / xhr.total * 100;
        console.log(Math.round(percentComplete) + '% downloaded');
    };
};
THREEX.Model.onError = function (xhr) {
    console.log('load error : ' + xhr);
};
THREEX.Model.load = function(url, mtlurl, onload, onProgress, onError) {
    var loader = new THREE.OBJMTLLoader();

    onProgress = THREEX.initVar(onProgress, function(xhr) {});
    onError = THREEX.initVar(onError, THREEX.Model.onError);
    loader.load(url, mtlurl, onload, onProgress, onError);
}

/**
 * Create a box3 model
 * @param bBox
 * @returns {THREE.Mesh}
 * @constructor
 */
THREEX.Model.CreateBoundingBoxModel= function (bBox) {
    var cubeGeo = new THREE.BoxGeometry(
        bBox.max.x - bBox.min.x,
        bBox.max.y - bBox.min.y,
        bBox.max.z - bBox.min.z);
    var mesh = new THREE.Mesh(cubeGeo, new THREE.MeshBasicMaterial({color : 0xff00ff, transparent: true, opacity: 0.25}));
    mesh.position.set(
        (bBox.max.x + bBox.min.x) / 2,
        (bBox.max.y + bBox.min.y) / 2,
        (bBox.max.z + bBox.min.z) / 2
    );
    return mesh;
}

/**
 *  This method is used to refresh a model on one place
 * @param placeId
 * @param infos
 *      //  Basic model infos
 *      id : model's id
 *      obj_path : model .obj file path
 *      mtl_pat : model .mtl file path
 *      scale [optional]: model scale params {x, y, z} , default is {1, 1, 1}
 *      rotation [optional] : model rotate params {x, y, z}, default is {0, 0, 0}
 *      castShadow [optional] : if model cast shadow to others, default is false
 *      receiveShadow [optional] : if model receive shadow from others, default is false
 *
 *      //  Interaction
 *      selectable [optional] : default is false, if true then open the interation on this model
 *      onClick [optional] : active when selectable, onClick function allow a function receive
 *          two params, (e, m)
 *          -   e equals event
 *          -   m queals bBoxModel, the only one thing you should know is that this contain infos
 *              about the modelId and placeId, so you can do other things, for example, change this
 *              place's model
 *
 * @param onProgress [optional]
 * @param onError [optional]
 *
 */
THREEX.Model.refresh = function(placeId, infos, onProgress, onError) {
    if (typeof(infos.objPath) == 'undefined'
            || typeof (infos.mtlPath) == 'undefined'
            || typeof(infos.id) == 'undefined'
            || typeof(scene) == 'undefined'
            || typeof(gUserInfo.models[placeId]) == 'undefined')
        return false;


    infos.scale = THREEX.initVar(infos.scale, { x : 1, y : 1, z :1 });
    infos.rotation = THREEX.initVar(infos.rotation, {x : 0, y : 0, z : 0});
    infos.castShadow = THREEX.initVar(infos.castShadow, false);
    infos.receiveShadow = THREEX.initVar(infos.receiveShadow, false);
    infos.onprogress = THREEX.initVar(infos.onprogress, THREEX.Model.onProgress);
    infos.onError = THREEX.initVar(infos.onError, THREEX.Model.onError);
    infos.selectable = THREEX.initVar(infos.selectable, false);
    infos.onClick = THREEX.initVar(infos.onClick, null);

    var onLoadModel = function(o) {
        var object;

        //  check o type
        if (o instanceof THREEX.ModelEntity)
            object = o.obj;
        else
            object = o;

        //  remove old model and boxModel
        var pn = gUserInfo.models[placeId];
        if (pn.object != null) {
            scene.remove(pn.object.obj);
            scene.remove(pn.object.bBoxModel);
        }

        object.position.set(pn.position.x, pn.position.y, pn.position.z);
        object.scale.set(infos.scale.x, infos.scale.y, infos.scale.z);
        object.rotation.set(infos.rotation.x, infos.rotation.y, infos.rotation.z);
        object.traverse(function(child) {
            if (child instanceof THREE.Mesh) {
                child.castShadow = infos.castShadow;
                child.receiveShadow = infos.receiveShadow;
            }
        });


        //  Compute the bBox and create bBox model
        var bBox = new THREE.Box3();
        bBox.setFromObject(object);

        var modelEntity = new THREEX.ModelEntity();
        modelEntity.obj = object;
        modelEntity.bBoxModel = THREEX.Model.CreateBoundingBoxModel(bBox);

        /*
            bBoxModel contains visible, modelId, selectable and placeId
            -   visible : default is false
            -   modelId : record the model Id information
            -   placeId : record where it and model set
            -   selectable : default is true
         */

        modelEntity.bBoxModel.visible = false;
        modelEntity.bBoxModel.modelId = infos.id;
        modelEntity.bBoxModel.placeId = placeId;
        modelEntity.bBoxModel.selectable = infos.selectable;
        modelEntity.bBoxModel.hovered = false;
        modelEntity.bBoxModel.onClick = infos.onClick;

        scene.add(modelEntity.obj);
        scene.add(modelEntity.bBoxModel);
        pn.object = modelEntity;

        //  Add models to global for check -- only one
        THREEX.Global.add(infos.id, modelEntity);

        //  Save object to cache for reuse -- need check
        if (THREEX.MemCache.checkRes(infos.objPath));
            THREEX.MemCache.saveRes(infos.objPath, modelEntity);

        THREEX.materialNeedsUpdate = true;

    };

    if (THREEX.MemCache.checkRes(infos.objPath)) {
        onLoadModel(THREEX.MemCache.loadRes(infos.objPath));
    } else
        THREEX.Model.load(infos.objPath, infos.mtlPath, onLoadModel, onProgress, onError);

    return true;
};

/**
 * This method is used to update a mesh's texture dynamically
 * @param mesh : this param can be a Mesh or meshIDString
 * @param texture : this param can be a Texture or textureURL
 * @param params
 *      wrap [optional] : struct is {s, t}, default are THREE.RepeatWrapping
 *      repeat [optional] : struct is {x, y}, repeat times in x and y, default are 1
 *      anisotropy [optional] : default is 1, max can be set to 16, the bigger the better
 *
 * Example :
 *  //  update a wall's texture
 *  THREEX.Model.UpdateTexture("meshFrontWall", "textures/wall/....", { });
 *
 *  //  you can also use mesh directly
 *  var meshFrontWall; // type is THREE.Mesh
 *  THREEX.Model.UpdateTexture(meshFrontWall, "textures/wall/....");
 */
THREEX.Model.updateModelTexture = function(mesh, texture, params) {
    var meshObj, textureObj;

    params.wrap = THREEX.initVar(params.wrap, { s : THREE.RepeatWrapping, t : THREE.RepeatWrapping });
    params.repeat = THREEX.initVar(params.repeat, { x : 1,y : 1 });
    params.anisotropy = THREEX.initVar(params.anisotropy, 1);

    THREEX.initVar(params.width, null);
    THREEX.initVar(params.height, null);

    //  distinguish URL or Mesh
    if (mesh instanceof THREE.Mesh)
        meshObj = mesh;
    else if (typeof (mesh) == 'string')
        meshObj = THREEX.Global.get(mesh);

    //  distinguish URL or Texture
    if (texture instanceof THREE.Texture)
        textureObj = texture;
    else if (typeof(texture) == 'string') {
        textureObj = THREEX.Texture.load(texture);
    }
    textureObj.wrapS = params.wrap.s;
    textureObj.wrapT = params.wrap.T;
    textureObj.repeat.set(params.repeat.x, params.repeat.y);
    textureObj.anisotropy = params.anisotropy;

    if (params.width != null && params.height != null)
        meshObj.scale.set(params.width / meshObj.geometry.parameters.width, params.height / meshObj.geometry.parameters.height, 1);

    meshObj.material.map = textureObj;
    THREEX.materialNeedsUpdate = true;
}

/**
 * Return the model's vertices count and faces count
 * @param model
 * @returns {{vertices: number, faces: number}}
 */
THREEX.Model.getModelVerticesAndFacesCount = function(model) {
    var ret = {vertices : 0, faces : 0};

    if (model instanceof THREE.Mesh) {
        if (typeof(model.geometry.vertices) != "undefined" && typeof(model.geometry.faces) != 'undefined') {
            ret.vertices += model.geometry.vertices.length;
            ret.faces += model.geometry.faces.length;
        }
    } else if (model instanceof THREEX.ModelEntity) {
        var obj3d = model.obj;

        obj3d.traverse(function(child) {
            if (child instanceof THREE.Mesh) {
                ret.vertices += child.geometry.vertices.length;
                ret.faces += child.geometry.faces.length;
            }
        })
    }

    return ret;
}