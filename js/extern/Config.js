/**
 * Created by woud on 2014/12/27.
 */

var gKeyboard = {
    Keys : {
        up : 87,    // w
        down : 83,  // s
        left : 65,  // a
        right : 68  // d
    },
    Move : {
        speed : 15
    }
};

var gSystem = {
    Screen : {
        x : 0,
        y : 0,
        width : 1920,
        height : 978
    },
    Mouse : { }
};

/**
 *  these data are closed to business
 */
var gBusiness = {
    world : {
        floorHeight : -250
    },
    initPos : new THREE.Vector3(-200, 0, 50),
    lockActiveArea : true,
    activeArea : {
        x : { min : -400, max : 400},
        z : { min : -400, max : 400}
    },
    lockViewAngle : true,
    viewAngle : {
        lat : {max : 85, min : -85},
        lon : {max : 60, min : -60}
    },
    preloadAnimate : false
}

/**
 *
 */
var gResource = {
    models : {
        'vaseModel' : { objUrl : 'obj/vases/vase.obj', mtlUrl : 'obj/vases/vase.mtl'}
    }
}