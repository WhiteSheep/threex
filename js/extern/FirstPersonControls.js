/**
 * Created by woud on 2015/2/1.
 */

THREEX.FirstPersonControls = function(camera, domElement) {
    this.camera = camera;
    this.target = new THREE.Vector3(0, 0, 0);
    this.domElement = (domElement !== undefined) ? domElement : document;
    this.keys = new Array(256);
    this.moveSpeed = 500;

    this.isUserInteracting = false;
    this.onPointerDownX = 0;
    this.onPointerDownY = 0;
    this.onPointerDownLon = 0;
    this.onPointerDownLat = 0;

    this.lat = 0
    this.lon = 0;
    this.theta = 0;
    this.phi = 0;

    this.latSpeed = 0.1;
    this.lonSpeed = 0.1;
    this.wheelSpeed = 0.02;

    //  init keys state
    for (var idx = 0; idx < this.keys.length; ++idx)
         this.keys[idx] = {pressed : false};

    this.handleResize = function() {
        gSystem.Screen.width = window.innerWidth;
        gSystem.Screen.height = window.innerHeight;

        this.camera.aspect = gSystem.Screen.width / gSystem.Screen.height;
        this.camera.updateProjectionMatrix();
    }

    this.onKeyDown = function(event) {
        this.keys[event.keyCode].pressed = true;
    }

    this.onKeyUp = function(event) {
        this.keys[event.keyCode].pressed = false;
    }

    this.onMouseDown = function(event) {
        event.preventDefault();
        this.isUserInteracting = true;
        this.onPointerDownX = event.clientX;
        this.onPointerDownY = event.clientY;
        this.onPointerDownLon = this.lon;
        this.onPointerDownLat = this.lat;

    }

    this.onMouseMove = function(event) {
        if (this.isUserInteracting === true) {
            this.lon = ( this.onPointerDownX - event.clientX ) * this.lonSpeed + this.onPointerDownLon;
            this.lat = ( event.clientY - this.onPointerDownY ) * this.latSpeed + this.onPointerDownLat;
        }
    }

    this.onMouseUp = function(event) {
        event.preventDefault();
        this.isUserInteracting = false;
    }

    this.onMouseWheel = function(event) {
        this.camera.fov -= event.wheelDeltaY * this.wheelSpeed;
        this.camera.updateProjectionMatrix();
    }

    this.update = function(delta) {
        /// Move
        var dir = new THREE.Vector3(0, 0, 0);

        if (this.keys[gKeyboard.Keys.up].pressed)
            dir.add(new THREE.Vector3(this.target.x - this.camera.position.x, 0, this.target.z - this.camera.position.z).normalize());
        if (this.keys[gKeyboard.Keys.down].pressed)
            dir.add(new THREE.Vector3(this.target.x - this.camera.position.x, 0, this.target.z - this.camera.position.z).normalize().negate());
        if (this.keys[gKeyboard.Keys.left].pressed)
            dir.add(new THREE.Vector3(this.target.z - this.camera.position.z, 0, - (this.target.x - this.camera.position.x)).normalize());
        if (this.keys[gKeyboard.Keys.right].pressed)
            dir.add(new THREE.Vector3(- (this.target.z - this.camera.position.z), 0, this.target.x - this.camera.position.x).normalize());

        this.camera.position.add(dir.multiplyScalar(this.moveSpeed * delta));

        //  lock the active area
        if (gBusiness.lockActiveArea) {
            if (this.camera.position.x > gBusiness.activeArea.x.max)
                this.camera.position.x = gBusiness.activeArea.x.max;
            else if (this.camera.position.x < gBusiness.activeArea.x.min)
                this.camera.position.x = gBusiness.activeArea.x.min;

            if (this.camera.position.z > gBusiness.activeArea.z.max)
                this.camera.position.z = gBusiness.activeArea.z.max;
            else if (this.camera.position.z < gBusiness.activeArea.z.min)
                this.camera.position.z = gBusiness.activeArea.z.min;
        }

        /// Change sight
        if (gBusiness.lockViewAngle) {
            this.lat = Math.max( gBusiness.viewAngle.lat.min, Math.min( gBusiness.viewAngle.lat.max, this.lat ) );
            this.phi = THREE.Math.degToRad( 90 - this.lat );
            this.theta = THREE.Math.degToRad(Math.max(gBusiness.viewAngle.lon.min, Math.min(gBusiness.viewAngle.lon.max, this.lon)));
        }

        this.target.x = this.camera.position.x + 500 * Math.sin( this.phi ) * Math.cos( this.theta );
        this.target.y = 500 * Math.cos( this.phi );
        this.target.z = this.camera.position.z + 500 * Math.sin( this.phi ) * Math.sin( this.theta );
        this.camera.lookAt( this.target );
        this.camera.updateMatrix();
    }

    this.domElement.addEventListener('mousedown', bind(this, this.onMouseDown), false);
    this.domElement.addEventListener('mouseup', bind(this, this.onMouseUp), false);
    this.domElement.addEventListener('mousemove', bind(this, this.onMouseMove), false);
    this.domElement.addEventListener('mousewheel', bind(this, this.onMouseWheel), false);

    window.addEventListener('keydown', bind(this, this.onKeyDown), false);
    window.addEventListener('keyup', bind(this, this.onKeyUp), false);

    function bind(scene, fn) {
        return function() {
            fn.apply(scene, arguments);
        }
    }
}